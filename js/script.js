// Опишите своими словами, что такое экранирование, и зачем оно нужно в языках программирования
//экранирование для спец символов в выражениях встречаются.

function createNewUser() {
    let fName = prompt("Enter First Name", 'Cristiano');
    // сделал по умолчанию данные что бы не вбивать посстоянно, я думаю в таких примерах такое не считается ошибкой?
    let lName = prompt("Enter last Name", 'Ronaldo');
    let birthday = (prompt('введите дату', '15.05.1986')); //текст в формате dd.mm.yyyy
    birthday = new Date(birthday.split(".").reverse().join("."));

    let newUser = {
        "firstName": fName,
        "lastName": lName,
        "birthday": birthday,
        getLogin() {
            return (this.firstName[0] + this.lastName).toLowerCase();
        },
        setFirstName(firstName) {
            Object.defineProperty(newUser, "firstName", {value: "Denis"})
        },
        setLastName(lastName) {
            Object.defineProperty(newUser, "lastName", {value: "Shapochkin"})
        },
        getAge() {
            let now = new Date();
            let age = now.getFullYear() - birthday.getFullYear();
            let month = now.getMonth() - birthday.getMonth();
            if (month < 0 || (month === 0 && now.getDate() < birthday.getDate())) {
                age = age - 1;
            }
            return age;
        },
        getPassword() {
            return (this.firstName[0] + this.lastName.toLowerCase() + this.birthday.getFullYear());
        }
    }
    Object.defineProperty(newUser, 'firstName', {
        configurable: true,
        writable: false,
        enumerable: true,
    });
    Object.defineProperty(newUser, 'lastName', {
        configurable: true,
        writable: false,
        enumerable: true,
    });
    return newUser;
}
const user = createNewUser();
// alert(user.firstName); //oldName
// alert(user.lastName); //oldlastName
// user.setFirstName('Denis');

alert(`пользователю ${user.getAge()} года(лет)`); // Denis newName
alert(user.getPassword());
// user.setLastName('Shapochkin');
// alert(user.lastName); // Shapochkin newName

